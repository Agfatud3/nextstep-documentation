Document Return: Delivery by HTTP PUSH
===============================================

Introduction
------------
The Document Delivery API enables integration with applications that are capable of hosting a Web API
endpoint that can receive requests from the NEXTSTEP platform.

Each document will be delivered to the provided endpoint via an HTTP PUSH request that will contain the
document image and metadata files.

The specific files that will be included in the delivery request are defined by the process configuration,
which should be be discovered, documented and implemented by the process owner or administrator prior to starting
the API integration.

Request Details
---------------
Requests from the platform for document delivery will always be an HTTP POST with a multipart form data
content type, containing the headers and content sections described below:

.. list-table:: **Headers & Form Content Sections**
   :header-rows: 1
   :widths: 20, 25, 55

   * - Name
     - Location
     - Sample Value & Description

   * - **Delivery URL**
     - Configuration
     - *https://api.your-domain.com/api/nextstep/delivery*

       During discovery and configuration of your NEXTSTEP process, you will need to provide
       the host name where delivery requests should be sent.

       *The values above are for example purposes only*

   * - **Authorization**
     - Header
     - *Bearer mF_9.B5f-4.1JqM*

       The authorization header will be populated with the static bearer token provided
       to the platform during configuration. (Optional)

       Currently, only bearer token authentication is supported.

   * - **<File Name>**
     - Content Sections
     - *<binary file data>*

       Each configured delivery file will be added to the request in a separate content section.
       Each section will include the customer specified name to represent the file, as well as a
       logical file name, with file type extension, and content type header value.

       The bytes representing the file content will be sent as the section body.

       See :ref:`Example <delivery-example>` below for a sample request that posts both an image
       file and metadata file to the delivery endpoint.

.. _delivery-example:

Example
-------
This HTTP POST request for document delivery (in raw text format) uses a sample URL and path, and
simulates delivery of a `TIFF`_ image and metadata file in `JSON`_ format. Other image and metadata formats
are also supported. See :doc:`../index` for additional image format details. Metadata files can be generated
in either `XML`_, or `JSON`_ format.

.. _TIFF: https://en.wikipedia.org/wiki/TIFF
.. _JSON: https://en.wikipedia.org/wiki/JSON
.. _XML: https://en.wikipedia.org/wiki/XML

Sample Request::

  POST /api/nextstep/delivery
  Host: https://api.your-domain.com/
  Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryWfPNVh4wuWBlyEyQ

  ------WebKitFormBoundaryWfPNVh4wuWBlyEyQ

  Content-Disposition: form-data; name="file"; filename="123456789.tif"
  Content-Type: image/tiff

  <binary file data>

  ------WebKitFormBoundaryWfPNVh4wuWBlyEyQ

  Content-Disposition: form-data; name="metadata"; filename="metadata.json"
  Content-Type: application/json

  {
    "sourceId": "123456789",
    "fields": [
      {
        "name": "patient_name",
        "value": "John Smith",
        "confidence": 0.8
      },
      {
        "name": "patient_dob",
        "value": "02/20/1964",
        "confidence": 1.0
      },
      {
        "name": "date_of_encounter",
        "value": "01/30/2018",
        "confidence": 0.95
      },
      {
        "name": "mrn",
        "value": "142637A",
        "confidence": 0.9
      }
    ]
  }

  ------WebKitFormBoundaryWfPNVh4wuWBlyEyQ

